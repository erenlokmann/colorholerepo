﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RandomMovement : MonoBehaviour
{   
    public List <GameObject> Objects;
    
    void Start()
    {
        Objects.InsertRange(0,GameObject.FindGameObjectsWithTag("SphereObstacle"));
        Objects.Sort((x,y) => Random.value < 0.5f ? -1 : 1);
        int movingPossibility = Random.Range(2, Objects.Count / 3);
        for (int i = 0; i < movingPossibility; i++)
        {
            int currentObject = Random.Range(0, Objects.Count);
            if (Objects[currentObject].GetComponent<Rigidbody>() == null) 
            {
                Objects[currentObject].AddComponent<Rigidbody>();
                Objects[currentObject].AddComponent<ObjectMovement>();
                Objects[currentObject].AddComponent<SphereCollider>();
                Objects[currentObject].AddComponent<SphereCollider>().isTrigger = true;
                Objects[currentObject].AddComponent<SphereCollider>().radius = 3f;
  
            }
        }
    }

    void Update()
    {
        
    }
}
