﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class ObjectMovement : MonoBehaviour {

    private bool moving = false;

    void OnTriggerEnter (Collider objects)
    {
        if (objects.tag == "MainCamera" && moving == false)
        {
            moving = true;
            Vector3 direction = new Vector3 (transform.position.x + Random.Range(-5,5), transform.position.y + Random.Range(0,0), transform.position.z + Random.Range(-5,5));
            gameObject.GetComponent<Rigidbody>().AddExplosionForce(Random.Range(200,2000),direction,5f);
        }
    }
void OnDrawGizmos()
{
    Gizmos.DrawWireSphere(transform.position,5f);
}
}